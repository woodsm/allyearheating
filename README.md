[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://github.com/mxttwoods/allyearheating)
![Node.js CI](https://github.com/mxttwoods/allyearheating/workflows/Node.js%20Build%20and%20Deploy/badge.svg)

# AllYear Heating and Cooling - Company Website

<p align="center">
  <a href="http://allyearheating.com/" target="blank"><img src="https://allyearheating.com/images/ALL-YEAR.png" width="400" alt="the allyearheating logo" /></a>
</p>

![SL Scan](https://github.com/mxttwoods/allyearheating/workflows/SL%20Scan/badge.svg)
![OSSAR](https://github.com/mxttwoods/allyearheating/workflows/OSSAR/badge.svg)
![Greetings](https://github.com/mxttwoods/allyearheating/workflows/Greetings/badge.svg)

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/045b884146ec43ef9e0152166a1e9b20)](https://www.codacy.com/manual/mattwoods9170/allyearheating?utm_source=github.com&utm_medium=referral&utm_content=mxttwoods/allyearheating&utm_campaign=Badge_Grade)
[![CodeFactor](https://www.codefactor.io/repository/github/mxttwoods/allyearheating/badge)](https://www.codefactor.io/repository/github/mxttwoods/allyearheating)
![Website](https://img.shields.io/website?url=https%3A%2F%2Fallyearheating.com)

![Uptime Robot ratio (30 days)](https://img.shields.io/uptimerobot/ratio/m785897375-7751fb1e890a4c13e169bb69?label=30%20Day%20Uptime)
![Uptime Robot ratio (7 days)](https://img.shields.io/uptimerobot/ratio/7/m785897375-7751fb1e890a4c13e169bb69?label=7%20Day%20Uptime)
![Uptime Robot status](https://img.shields.io/uptimerobot/status/m785897375-7751fb1e890a4c13e169bb69?label=%205min%20Status)

![GitHub repo size](https://img.shields.io/github/repo-size/mxttwoods/allyearheating)
![GitHub code size in bytes](https://img.shields.io/github/languages/code-size/mxttwoods/allyearheating)
![GitHub file size in bytes](https://img.shields.io/github/size/mxttwoods/allyearheating/src/index.ejs)

![GitHub last commit](https://img.shields.io/github/last-commit/mxttwoods/allyearheating)
![GitHub language count](https://img.shields.io/github/languages/count/mxttwoods/allyearheating)
![GitHub top language](https://img.shields.io/github/languages/top/mxttwoods/allyearheating)

## Summary

A static website for a Local HVAC Company and Friend.
Built with:

- 11ty
- Ejs
- Firebase
- HTML5
- CSS3
- Bootstrap
- Jquery
- Javascript

### Philosophy

- Minimal code (HTML, CSS & JS). Add what you need
- SEO-friendly
- 🚀 Production-ready

### Requirements

- Node.js and npm

## Authors

- **Matthew Woods** - _Developer_ - [Matthew](https://github.com/mxttwoods)

### License

Licensed under the MIT License, Copyright © 2020

See [LICENSE](LICENSE) for more information.
