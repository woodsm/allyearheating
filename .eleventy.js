const htmlmin = require('html-minifier');
const lazyImagesPlugin = require('eleventy-plugin-lazyimages');

module.exports = function (eleventyConfig) {
    eleventyConfig.setEjsOptions({
        rmWhitespace: true,
    });
    eleventyConfig.addPlugin(lazyImagesPlugin, {
        imgSelector: '.lazyimages img',
        input: 'src/images',
    });
    eleventyConfig.addPassthroughCopy('src/images');
    eleventyConfig.addPassthroughCopy('src/robots.txt');
    eleventyConfig.addPassthroughCopy('src/assets');
    eleventyConfig.addTransform('htmlmin', (content, outputPath) => {
        if (outputPath.endsWith('.html')) {
            const minified = htmlmin.minify(content, {
                minifyCSS: true,
                removeTagWhitespace: true,
                minifyJS: true,
                useShortDoctype: true,
                removeComments: true,
                collapseWhitespace: true,
            });
            return minified;
        }
        return content;
    });
    return {
        dir: {input: 'src', output: '_site', data: '_data'},
    };
};
