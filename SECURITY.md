# Security Policy

## Supported Versions

| Version | Supported          |
| ------- | ------------------ |
| 1.3.x   | :white_check_mark: |
| < 1.2.x | :x:                |

## Reporting a Vulnerability

If a vulnetability is found please open a issue on GitHub.

You can expect a response ASAP from contributors.
